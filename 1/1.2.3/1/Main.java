import java.util.Scanner;
import java.util.Locale;

public class Main {
    public static void main(String []args) {
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println(String.format("%7.3f", sc.nextDouble()));
    }
}
