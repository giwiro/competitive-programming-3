#include <cstdio>
#define PI 3.1415926535897932

int main() {
    int n;
    scanf("%d", &n);
    printf("%.*f", n, PI);
    return 0;
}
