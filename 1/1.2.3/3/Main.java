import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Main {
    public static void main(String []args) {
        Scanner sc = new Scanner(System.in);
        Integer day = sc.nextInt();
        Integer month = sc.nextInt();
        Integer year = sc.nextInt();
        Calendar cal = new GregorianCalendar();
        cal.set(GregorianCalendar.YEAR, year);
        cal.set(GregorianCalendar.MONTH, month);
        cal.set(GregorianCalendar.DATE, day);
        System.out.println(cal.get(Calendar.DAY_OF_WEEK));

    }
}
