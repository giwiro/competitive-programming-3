#include <cstdio>
#include <algorithm>
#include <vector>
#define ALL(x) x.begin(), x.end()

using namespace std;

int main() {
    vector<int> v = {10, 10, 10, 48, 2, 19, 19, 4, 4, 4};
    sort(ALL(v));
    v.resize(unique(ALL(v)) - v.begin());
    for (int i = 0; i < (int)v.size(); i++) {
        printf("%d\n", v[i]);
    }
    return 0;
}
